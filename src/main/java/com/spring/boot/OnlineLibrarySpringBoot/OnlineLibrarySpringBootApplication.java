package com.spring.boot.OnlineLibrarySpringBoot;

import com.spring.boot.OnlineLibrarySpringBoot.repository.UserRepository;
import org.jsondoc.spring.boot.starter.EnableJSONDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

// Project Link: https://github.com/oktayuyar/SimpleLibraryAutomation

/*
 The entry point of the spring boot application is the class which contains
       - @SpringBootApplication annotation and
       - the main method.

@SpringBootApplication annotation includes
    - Auto- Configuration,
    - Component Scan, and
    - Spring Boot Configuration
 */

@ComponentScan(basePackages = {"com.spring.boot.OnlineLibrarySpringBoot"})
@EnableJpaRepositories(basePackages = "com.spring.boot.OnlineLibrarySpringBoot")
@EntityScan("com.spring.boot.OnlineLibrarySpringBoot.model")
@EnableJSONDoc
@SpringBootApplication
@EnableTransactionManagement
public class OnlineLibrarySpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineLibrarySpringBootApplication.class, args);

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10); // Strength set as 16
        String encodedPassword = encoder.encode("bela");
        System.out.println("BCryptPasswordEncoder");
        System.out.println(encodedPassword);
        System.out.println("\n");

    }

}
