package com.spring.boot.OnlineLibrarySpringBoot.controllers;

import com.spring.boot.OnlineLibrarySpringBoot.model.Customer;
import com.spring.boot.OnlineLibrarySpringBoot.services.CustomerService;
import com.spring.boot.OnlineLibrarySpringBoot.util.WriteCsvToResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping(value = "/customerView/list_customers")
    public ModelAndView getAllCustomers(ModelAndView model){
        model.addObject("listCustomers", customerService.getAllCustomers());
        model.setViewName("customers");
        return model;
    }

    @GetMapping(value = "/customerView/add_customer", produces = "application/json")
    public ModelAndView addCustomer(ModelAndView model){
        Customer customer = new Customer();
        customer.setDateCreated(new Date());
        model.addObject("customer", customer);
        model.setViewName("addCustomer");
        return model;
    }

    @PostMapping(value = "customerView/save_customer", produces = "application/json")
    public ModelAndView saveCustomer(@ModelAttribute Customer customer, @RequestParam(value = "action") String action) {
        ModelAndView model = new ModelAndView();
        // if save button is pressed (not cancel button)
        if (action.equals("save")) {
            if (customer.getId() == null) {     // new user to be added
                customerService.addCustomer(customer);
                model.addObject("operation", "add");
            } else {
                customerService.updateCustomer(customer.getId(), customer);
                model.addObject("operation", "update");
            }
        }
        model.setViewName("redirect:/customerView/list_customers");
        return model;
    }

    @GetMapping(value = "customerView/update_customer", produces = "application/json")
    //@ApiMethod(description = "Edit a user from the database")
    public ModelAndView updateCustomer(HttpServletRequest request, ModelAndView model) {
        Integer id = Integer.parseInt(request.getParameter("id"));
        Customer customer = customerService.getCustomerById(id);

        model.setViewName("addCustomer");
        model.addObject("customer", customer);
        return model;
    }

    @GetMapping(value = "customerView/delete_customer")
    public ModelAndView deleteCustomer(Model model, HttpServletRequest request){
        Integer customerId = Integer.parseInt(request.getParameter("id"));
        customerService.deleteCustomer(customerId);
        return new ModelAndView("redirect:/customerView/list_customers");
    }

    @PostMapping(value = "customerView/search_customer", produces = "application/json")
    public ModelAndView searchCustomer(ModelAndView model, @RequestParam(value = "customerSearchByCnp") String customerSearchByCnp) {
        List<Customer> customers = customerService.findByCnpContaining(customerSearchByCnp);
        model.addObject("customerSearchByCnp", customerSearchByCnp);
        model.addObject("listCustomers", customers);
        model.setViewName("customers");
        return model;
    }

    // LINK: https://attacomsian.com/blog/export-download-data-csv-file-spring-boot
    // LINK: http://zetcode.com/articles/springbootcsv/
    @GetMapping(value = "/customerView/export_customers", produces = "text/csv")
    public void exportCustomers(HttpServletResponse response) throws IOException {

        //set file name and content type
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + "customers.csv" + "\"");

        new WriteCsvToResponse().writeValuesListToCsv(response.getWriter(),(List<Customer>)customerService.getAllCustomers());
    }
}
