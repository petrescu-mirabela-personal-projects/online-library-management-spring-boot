package com.spring.boot.OnlineLibrarySpringBoot.controllers;

import com.spring.boot.OnlineLibrarySpringBoot.model.Author;
import com.spring.boot.OnlineLibrarySpringBoot.model.Item;
import com.spring.boot.OnlineLibrarySpringBoot.model.ItemProfile;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.ItemStatus;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.ItemType;
import com.spring.boot.OnlineLibrarySpringBoot.repository.ItemRepository;
import com.spring.boot.OnlineLibrarySpringBoot.services.ItemRankService;
import com.spring.boot.OnlineLibrarySpringBoot.services.ItemService;
import com.spring.boot.OnlineLibrarySpringBoot.util.WriteCsvToResponse;
import org.jsondoc.core.annotation.ApiMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Controller
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private ItemRankService itemRankService;

    @GetMapping(value = "/itemView")
    @ApiMethod(description = "Librarian View")
    public String itemView() {
        return "itemView";
    }

    @GetMapping(value = "/itemView/list_items", produces = "application/json")
    public ModelAndView listItems(ModelAndView model){
        model.addObject("listItems", itemService.getAllItems());
        model.setViewName("items");
        return model;
    }

    @GetMapping(value = "itemView/register_item", produces = "application/json")
    @ApiMethod(description = "Add a item to the database")
    public ModelAndView addItem(ModelAndView model) {
        Item item = new Item();
        item.setItemStatus(ItemStatus.AVAILABLE);
        ItemProfile itemProfile = new ItemProfile();
        itemProfile.setRegisteredYear(new Date());
        item.setItemProfile(itemProfile);
        model.addObject("item", item);
        model.addObject("allItemRank", itemRankService.getAllItemRank());
        model.addObject("allAuthors", itemService.getAllItems());
        model.setViewName("registerItem");
        return model;
    }

    @PostMapping(value = "itemView/save_item", produces = "application/json")
    //@ApiMethod(description = "Save a item to the database")
    public ModelAndView saveItem(@ModelAttribute Item item, @RequestParam(value = "action") String action) {
        ModelAndView model = new ModelAndView();
        // if save button is pressed (not cancel button)
        if (action.equals("save")) {
            if (item.getId() == null) {     // new item to be added
                itemService.addItem(item);
                model.addObject("operation", "add");
            } else {
                itemService.updateItem(item.getId(), item);
                model.addObject("operation", "update");
            }
        }
        model.setViewName("redirect:/itemView");

        return model;
    }

    @GetMapping(value = "itemView/update_item", produces = "application/json")
    //@ApiMethod(description = "Edit a item from the database")
    public ModelAndView updateItem(HttpServletRequest request, ModelAndView model) {
        Integer id = Integer.parseInt(request.getParameter("id"));
        Item item = itemService.getItemById(id);

        model.setViewName("registerItem");
        model.addObject("item", item);
        model.addObject("allItemRank", itemRankService.getAllItemRank());
        return model;
    }

//    @PostMapping(value = "itemView/item_catalog", produces = "application/json")
//    //@ApiMethod(description = "Search a item from the database by different criterias")
//    public ModelAndView itemCatalog(ModelAndView model,
//                                    @RequestParam(value = "itemSearchByTitle") String itemSearchByTitle,
//                                    @RequestParam(value = "itemSearchByAuthor") String itemSearchByAuthor,
//                                    @RequestParam(value = "itemSearchByRank") String itemSearchByRank,
//                                    @RequestParam(value = "itemSearchByRegistrationYear") String itemSearchByRegistrationYear,
//                                    @RequestParam(value = "itemSearchByPublishedYear") String itemSearchByPublishedYear) {
//        List<Item> items = itemService.findByTitleContaining(itemSearch);
//        model.addObject("itemSearchByTitle", itemSearch);
//        model.addObject("listItems", items);
//        model.setViewName("items");
//        return model;
//    }

    @PostMapping(value = "itemView/search_item_by_title", produces = "application/json")
    //@ApiMethod(description = "Search a user from the database by title")
    public ModelAndView searchItemByTitle(ModelAndView model, @RequestParam(value = "itemSearchByTitle") String itemSearch) {
        List<Item> items = itemService.findByTitleContaining(itemSearch);
        model.addObject("itemSearchByTitle", itemSearch);
        model.addObject("listItems", items);
        model.setViewName("items");
        return model;
    }

    @PostMapping(value = "itemView/search_item_by_type", produces = "application/json")
    //@ApiMethod(description = "Search a user from the database by type")
    public ModelAndView searchItemByType(ModelAndView model, @RequestParam(value = "itemSearchByType") ItemType itemSearch) {
        List<Item> items = itemService.findByItemType(itemSearch);
        model.addObject("itemSearchByType", itemSearch);
        model.addObject("listItems", items);
        model.setViewName("items");
        return model;
    }

    @PostMapping(value = "itemView/search_item_before_registered_year", produces = "application/json")
    //@ApiMethod(description = "Search a user from the database before registered year")
    public ModelAndView searchItemByRegisteredYear(ModelAndView model, HttpServletRequest request) throws ParseException {
        Date itemSearch = new SimpleDateFormat("yyyy-mm-dd").parse(request.getParameter("itemSearchBeforeRegisteredYear"));
        List<Item> items = itemService.findBeforeRegisteredYear(itemSearch);
        model.addObject("itemSearchBeforeRegisteredYear", itemSearch);
        model.addObject("listItems", items);
        model.setViewName("items");
        return model;
    }

    // LINK: https://attacomsian.com/blog/export-download-data-csv-file-spring-boot
    // LINK: http://zetcode.com/articles/springbootcsv/
    @GetMapping(value = "/itemView/export_items", produces = "text/csv")
    public void exportItems(HttpServletResponse response) throws IOException {

        //set file name and content type
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + "items.csv" + "\"");

        new WriteCsvToResponse().writeValuesListToCsv(response.getWriter(),(List<Item>)itemService.getAllItems());
    }
}
