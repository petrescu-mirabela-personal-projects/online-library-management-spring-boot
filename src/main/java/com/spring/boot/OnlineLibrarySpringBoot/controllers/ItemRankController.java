package com.spring.boot.OnlineLibrarySpringBoot.controllers;

import com.spring.boot.OnlineLibrarySpringBoot.model.Item;
import com.spring.boot.OnlineLibrarySpringBoot.model.ItemProfile;
import com.spring.boot.OnlineLibrarySpringBoot.model.ItemRank;
import com.spring.boot.OnlineLibrarySpringBoot.services.ItemRankService;
import com.spring.boot.OnlineLibrarySpringBoot.services.ItemService;
import com.spring.boot.OnlineLibrarySpringBoot.util.WriteCsvToResponse;
import org.jsondoc.core.annotation.ApiMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
public class ItemRankController {

    @Autowired
    private ItemRankService itemRankService;

    @GetMapping(value = "/itemView/list_item_rank", produces = "application/json")
    public ModelAndView listItemRank(ModelAndView model) {
        model.addObject("listItemRank", itemRankService.getAllItemRank());
        model.setViewName("itemRank");
        return model;
    }

    @GetMapping(value = "itemView/add_item_rank", produces = "application/json")
    @ApiMethod(description = "Add a item rank to the database")
    public ModelAndView addItemRank(ModelAndView model) {
        ItemRank itemRank = new ItemRank();
        model.addObject("itemRank", itemRank);
        model.setViewName("addItemRank");
        return model;
    }

    @PostMapping(value = "itemView/save_item_rank", produces = "application/json")
    //@ApiMethod(description = "Save a item to the database")
    public ModelAndView saveItemRank(@ModelAttribute ItemRank itemRank, @RequestParam(value = "action") String action) {
        ModelAndView model = new ModelAndView();
        // if save button is pressed (not cancel button)
        if (action.equals("save")) {
            if (itemRank.getId() == null) {     // new item rank to be added
                itemRankService.addItemRank(itemRank);
                model.addObject("operation", "add");
            } else {
                itemRankService.updateItemRank(itemRank);
                model.addObject("operation", "update");
            }
        }
        model.setViewName("redirect:/itemView");

        return model;
    }

    @GetMapping(value = "itemView/update_item_rank", produces = "application/json")
    //@ApiMethod(description = "Edit a item rank from the database")
    public ModelAndView updateItem(HttpServletRequest request, ModelAndView model) {
        Integer id = Integer.parseInt(request.getParameter("id"));
        ItemRank itemRank = itemRankService.findById(id);

        model.addObject("itemRank", itemRank);
        model.setViewName("addItemRank");
        return model;
    }

}
