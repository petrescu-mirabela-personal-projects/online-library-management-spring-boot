package com.spring.boot.OnlineLibrarySpringBoot.controllers;

import com.spring.boot.OnlineLibrarySpringBoot.model.User;
import com.spring.boot.OnlineLibrarySpringBoot.model.UserDetailsImpl;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.AuthorityType;
import com.spring.boot.OnlineLibrarySpringBoot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.support.SessionStatus;

// Link: https://dzone.com/articles/spring-security-5-form-login-with-database-provide

@SessionAttributes({"currentUser", "currentUserId"})
@Controller
public class LoginController {

    private static final Logger log = LogManager.getLogger(LoginController.class);

    @GetMapping({"/", "/index"})
    public String index() {
        return "index";
    }

    @GetMapping(value = "/login")
    public String login() {
        return "login";
    }

    @GetMapping(value = "/loginFailed")
    public String loginError(Model model) {
        log.info("/loginFailed ==> Login attempt failed");
        model.addAttribute("error", "true");
        return "login";
    }

    @PostMapping(value = "/postLogin")
    public String postLogin(Model model, HttpSession session) {
        log.info("/postLogin ==> postLogin()");

        // read principal out of security context and set it to session
        UsernamePasswordAuthenticationToken authentication =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        validatePrinciple(authentication.getPrincipal());
        User loggedInUser = ((UserDetailsImpl) authentication.getPrincipal()).getUserDetails();

        model.addAttribute("currentUser", loggedInUser.getUsername());
        model.addAttribute("currentUserId", loggedInUser.getId());
        session.setAttribute("userId", loggedInUser.getId());

        if (checkUserRole(loggedInUser, AuthorityType.ROLE_ADMIN)) {
            return "redirect:/adminView";
        } else if (checkUserRole(loggedInUser, AuthorityType.ROLE_USER)) {
            return "redirect:/itemView";
        } else
            return "redirect:/login";

    }

    // check the user role
    private boolean checkUserRole(User loggedInUser, AuthorityType roleUser) {
        return loggedInUser.getAuthorities().stream().anyMatch(s -> s.getName().equals(roleUser));
    }

    @GetMapping(value = "/logout")
    public String logout(SessionStatus session) {
        SecurityContextHolder.getContext().setAuthentication(null);
        session.setComplete();
        return "redirect:/login";
    }

    private void validatePrinciple(Object principal) {
        if (!(principal instanceof UserDetailsImpl)) {
            throw new IllegalArgumentException("Principal can not be null!");
        }
    }

    @RequestMapping("/error-forbidden")
    public String errorForbidden() {
        return "error-forbidden";
    }
}
