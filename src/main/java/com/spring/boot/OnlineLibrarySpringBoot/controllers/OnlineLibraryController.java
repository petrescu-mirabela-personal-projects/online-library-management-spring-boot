package com.spring.boot.OnlineLibrarySpringBoot.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class OnlineLibraryController {

    @RequestMapping("/hello")
    public String hello(){
        return "Hello";
    }
}
