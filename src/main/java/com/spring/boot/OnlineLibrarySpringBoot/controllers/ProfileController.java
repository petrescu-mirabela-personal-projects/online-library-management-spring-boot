package com.spring.boot.OnlineLibrarySpringBoot.controllers;

import com.spring.boot.OnlineLibrarySpringBoot.model.Profile;
import com.spring.boot.OnlineLibrarySpringBoot.model.User;
import com.spring.boot.OnlineLibrarySpringBoot.services.ProfileService;
import com.spring.boot.OnlineLibrarySpringBoot.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;

@Controller
public class ProfileController {

    @Autowired
    ProfileService profileService;

    @PostMapping(value = "adminView/save_profile", produces = "application/json")
    public ModelAndView saveProfile(@ModelAttribute Profile profile) {
        if (profile.getId() == null) {
            profileService.addProfile(profile);
        } else {
            profileService.updateProfile(profile.getId(), profile);
        }
        return new ModelAndView("redirect:/adminView/list_users");
    }

    @GetMapping(value = "adminView/update_profile", produces = "application/json")
    public ModelAndView updateProfile(HttpServletRequest request) {
        Integer id = Integer.parseInt(request.getParameter("id"));
        Profile profile = profileService.getProfileById(id);

        ModelAndView model = new ModelAndView("addProfile");
        model.addObject("profile", profile);
        return model;
    }

}
