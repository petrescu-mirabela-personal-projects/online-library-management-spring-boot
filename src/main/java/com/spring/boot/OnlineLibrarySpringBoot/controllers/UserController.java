package com.spring.boot.OnlineLibrarySpringBoot.controllers;

import com.spring.boot.OnlineLibrarySpringBoot.model.Authority;
import com.spring.boot.OnlineLibrarySpringBoot.model.Profile;
import com.spring.boot.OnlineLibrarySpringBoot.model.User;
import com.spring.boot.OnlineLibrarySpringBoot.model.UserDetailsImpl;
import com.spring.boot.OnlineLibrarySpringBoot.services.AuthorityService;
import com.spring.boot.OnlineLibrarySpringBoot.services.UserService;
import com.spring.boot.OnlineLibrarySpringBoot.util.WriteCsvToResponse;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.pojo.ApiStage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@Api(name = "User Library API",
        description = "Provide a list of methods that manage the users library",
        stage = ApiStage.RC)
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping(value = "/adminView")
    @ApiMethod(description = "Administrator View")
    public String adminView() {
        return "adminView";
    }

    @GetMapping(value = "adminView/list_users", produces = "application/json")
    @ApiMethod(description = "Get all users from the database")
    public ModelAndView getUsers(ModelAndView model) {
        Iterable<User> users = userService.getAllUsers();
        model.addObject("listUsers", users);
        model.setViewName("users");
        return model;
    }

    @GetMapping(value = "adminView/add_user", produces = "application/json")
    @ApiMethod(description = "Add a user to the database")
    public ModelAndView addUser(ModelAndView model) {
        User user = new User();
        user.setProfile(new Profile());
        user.setDateCreated(new Date());
        model.addObject("user", user);
        model.addObject("allAuthorities", authorityService.getAllAuthorities());
        model.setViewName("addUser");
        return model;
    }

    @PostMapping(value = "adminView/save_user", produces = "application/json")
    //@ApiMethod(description = "Save a user to the database")
    public ModelAndView saveUser(@ModelAttribute User user, @RequestParam(value = "action") String action) {
        ModelAndView model = new ModelAndView();
        // if save button is pressed (not cancel button)
        if (action.equals("save")) {
            if (user.getId() == null) {     // new user to be added
                /* encode the password on registration
                   https://www.baeldung.com/spring-security-registration-password-encoding-bcrypt */
                user.setPassword(encodePassword(user.getPassword()));
                userService.addUser(user);
                model.addObject("operation", "add");
            } else {
                //profileService.updateProfile(user.getId(), user.getProfile());
                user.setPassword(encodePassword(user.getPassword()));
                userService.updateUser(user.getId(), user);
                model.addObject("operation", "update");
            }
        }
        model.setViewName("redirect:/adminView/list_users");

        return model;
    }

    // encode the user password
    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    @GetMapping(value = "adminView/update_user", produces = "application/json")
    //@ApiMethod(description = "Edit a user from the database")
    public ModelAndView updateUser(HttpServletRequest request, ModelAndView model) {
        Integer id = Integer.parseInt(request.getParameter("id"));
        User user = userService.getUserById(id);
        //Profile profile = profileService.getProfileById(user.getId());
        //profile.setId(id);
        //user.setProfile(profile);

        model.setViewName("addUser");
        model.addObject("user", user);
        model.addObject("authorityId", user.getAuthorities().stream().map(Authority::getId).findFirst());
        model.addObject("allAuthorities", authorityService.getAllAuthorities());
        return model;
    }


    @GetMapping(value = "adminView/delete_user", produces = "application/json")
    //@ApiMethod(description = "Delete a user from the database")
    public ModelAndView deleteUser(HttpServletRequest request) {
        if (userService.getAllAdmins().size() > 1) {
            Integer userId = Integer.parseInt(request.getParameter("id"));
            // if the user is deleted, then also the corresponding profile will be deleted (automatically)
            userService.deleteUser(userId);
        }
        return new ModelAndView("redirect:/adminView/list_users");
    }

    @PostMapping(value = "adminView/search_user", produces = "application/json")
    //@ApiMethod(description = "Search a user from the database")
    public ModelAndView searchUser(ModelAndView model, @RequestParam(value = "userSearch") String userSearch) {
        List<User> users = userService.findByUsernameContaining(userSearch);
        model.addObject("userSearch", userSearch);
        model.addObject("listUsers", users);
        model.setViewName("users");
        return model;
    }

    @PostMapping(value = "adminView/sort_users", produces = "application/json")
    //@ApiMethod(description = "Sort the users by different criteria")
    public ModelAndView sortUsers(ModelAndView model, @RequestParam(value = "comparator") String[] compCriteria) {
        List<User> users = userService.sortUsers(compCriteria);
        model.addObject("listUsers", users);
        model.setViewName("users");
        return model;
    }

    @PostMapping(value = "/login/create_user_account", produces = "application/json")
    @ApiMethod(description = "Create user account")
    public ModelAndView createUserAccount(ModelAndView model, @Valid User user, BindingResult result) {
        if (result.hasErrors()) {
            model.setViewName("login");
            return model;
        }

        userService.addUser(user);
        model.addObject("allAuthorities", authorityService.getAllAuthorities());
        model.setViewName("login");
        return model;
    }

    @GetMapping(value = "adminView/change_password")
    public ModelAndView displayChangedPassword(ModelAndView modelAndView, User user) {
        modelAndView.addObject("user", user);
        modelAndView.setViewName("changePassword");
        return modelAndView;
    }

    @PostMapping(value = "adminView/change_password")
    public ModelAndView changePassword(HttpServletRequest request) {
        //String username = request.getUserPrincipal().getName();
        // read principal out of security context and set it to session
        UsernamePasswordAuthenticationToken authentication =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        validatePrinciple(authentication.getPrincipal());
        // loggedIn username
        String username = ((UserDetailsImpl) authentication.getPrincipal()).getUserDetails().getUsername();
        String oldPassword = request.getParameter("old_password");
        String newPassword = request.getParameter("new_password");
        String newPasswordConfirm = request.getParameter("new_password_confirm");
        User user = userService.findByUsername(username);
        if (user != null && username.equals(user.getUsername())) {
            //if(encodePassword(oldPassword).equals(user.getPassword()) &&
            //    newPassword.equals(newPasswordConfirm)){
            user.setPassword(encodePassword(newPassword));
            userService.updateUser(user.getId(), user);
            //}
        } else {
            return new ModelAndView("redirect:/adminView/change_password");
        }
        return new ModelAndView("redirect:/login");
    }

    // LINK: https://attacomsian.com/blog/export-download-data-csv-file-spring-boot
    // LINK: http://zetcode.com/articles/springbootcsv/
    @GetMapping(value = "adminView/export_users", produces = "text/csv")
    //@ApiMethod(description = "Export users from the database")
    public void exportUsers(HttpServletResponse response) throws IOException {

        //set file name and content type
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + "users.csv" + "\"");

        new WriteCsvToResponse().writeValuesListToCsv(response.getWriter(), userService.getAllUsers());
    }

    private void validatePrinciple(Object principal) {
        if (!(principal instanceof UserDetailsImpl)) {
            throw new IllegalArgumentException("Principal can not be null!");
        }
    }

}