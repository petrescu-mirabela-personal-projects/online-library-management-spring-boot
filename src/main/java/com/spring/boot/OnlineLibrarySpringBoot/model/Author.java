package com.spring.boot.OnlineLibrarySpringBoot.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "author", schema = "wantsome")
public class Author implements Serializable {

    private static final long serialVersionUID = -6790693372846798580L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty
    private String name;

//    /*
//    Note, that using @JoinTable, or even @JoinColumn isn't required: JPA will generate the table and column names for us.
//    However, the strategy JPA uses won't always match the naming conventions we use.
//    Hence the possibility to configure table and column names.
//     */
//    @ManyToMany
//    @JoinTable(
//            name = "item_author", schema = "wantsome",
//            joinColumns = @JoinColumn(name = "author_id"),
//            inverseJoinColumns = @JoinColumn(name = "item_id"))
//    private Set<Item> items;

    @ManyToMany(mappedBy = "authors")
    private Set<Item> items = new HashSet<>();

    public Author() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return Objects.equals(id, author.id) &&
                Objects.equals(name, author.name) &&
                Objects.equals(items, author.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, items);
    }
}
