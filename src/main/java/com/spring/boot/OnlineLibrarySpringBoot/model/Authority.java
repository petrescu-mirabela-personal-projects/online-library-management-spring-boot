package com.spring.boot.OnlineLibrarySpringBoot.model;

import com.spring.boot.OnlineLibrarySpringBoot.model.types.AuthorityType;

import javax.persistence.*;

@Entity
@Table(name = "authority", schema = "wantsome")
public class Authority {

    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private AuthorityType name;

    public Authority() {
    }

    public Authority(AuthorityType name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AuthorityType getName() {
        return name;
    }

    public void setName(AuthorityType name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + " ";
    }

}
