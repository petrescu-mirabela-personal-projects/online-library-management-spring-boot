package com.spring.boot.OnlineLibrarySpringBoot.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "history", schema = "wantsome")
public class History {

    @Id
    private Integer id;

    @Column(name = "item_id")
    private Integer itemId;

    @Column(name = "customer_id")
    private Integer customerId;

    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;         // the date when the item was lent

    @Column(name = "due_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dueDate;           // the dte when the item shall be returned

    @Column(name = "return_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date returnDate;       // the date when the item was returned

    public History() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        History history = (History) o;
        return Objects.equals(id, history.id) &&
                Objects.equals(itemId, history.itemId) &&
                Objects.equals(customerId, history.customerId) &&
                Objects.equals(startDate, history.startDate) &&
                Objects.equals(dueDate, history.dueDate) &&
                Objects.equals(returnDate, history.returnDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, itemId, customerId, startDate, dueDate, returnDate);
    }
}
