package com.spring.boot.OnlineLibrarySpringBoot.model;

import com.spring.boot.OnlineLibrarySpringBoot.model.types.ItemStatus;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.ItemType;
import jdk.nashorn.internal.objects.annotations.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "item", schema = "wantsome")
public class Item implements Serializable {

    //private static final long serialVersionUID = -6067986234568847019L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty
    private String title;

    /*
    many-to-many database association includes two parent tables which are linked through a
    third one containing two Foreign Keys referencing the parent tables.
     */
    @ManyToMany(
            //fetch = FetchType.EAGER)
            fetch = FetchType.LAZY)
    @JoinTable(
            name = "item_author", schema = "wantsome",
            joinColumns = {@JoinColumn(name = "item_id")},
            inverseJoinColumns = {@JoinColumn(name = "author_id")})
    private Set<Author> authors = new HashSet<>(3);

    // JPA will use the Enum.ordinal() value when persisting a given entity in the database
    // LINK: https://www.baeldung.com/jpa-persisting-enums-in-jpa
    @Column(name = "item_type")
    @Enumerated(EnumType.STRING)
    private ItemType itemType;

    @Column(name = "item_status")
    @Enumerated(EnumType.STRING)
    private ItemStatus itemStatus;

    // where the item is stored in the library
    @ManyToOne(fetch = FetchType.LAZY)
    private ItemRank itemRank;

    /*
     https://www.baeldung.com/jpa-one-to-one - Shared Primary Key in JPA
     https://vladmihalcea.com/jpa-hibernate-synchronize-bidirectional-entity-associations/

     >>>> The profile @OneToOne association is marked with the mappedBy attribute which indicates that
          the Profile side is responsible for handling this bidirectional association.
     >>>> If a Child entity is dissociated from its Parent, the Child Foreign Key is set to NULL.
          If we want to have the Child row deleted as well, we have to use the orphan removal support.
    */
    @OneToOne(
            mappedBy = "item",
            cascade = CascadeType.ALL,
            //orphanRemoval = true,
            optional = false,
            fetch = FetchType.LAZY)
    // @Basic being implicitly implied (For basic type attributes, the implicit naming rule is that the column name is the same as the attribute name.)
    private ItemProfile itemProfile;

    public Item() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public ItemStatus getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(ItemStatus itemStatus) {
        this.itemStatus = itemStatus;
    }

    public ItemProfile getItemProfile() {
        return itemProfile;
    }

    public void setItemProfile(ItemProfile itemProfile) {
        if (itemProfile == null) {
            if (this.itemProfile != null) {
                this.itemProfile.setItem(null);
            }
        } else {
            itemProfile.setItem(this);
        }
        this.itemProfile = itemProfile;
    }

    public ItemRank getItemRank() {
        return itemRank;
    }

    public void setItemRank(ItemRank itemRank) {
//        if(itemRank == null){
//            if(this.itemRank != null){
//                this.itemRank.setItem(null);
//            }
//        }
//        else{
//            itemRank.setItem(this);
//        }
        this.itemRank = itemRank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(id, item.id) &&
                Objects.equals(title, item.title) &&
                Objects.equals(authors, item.authors) &&
                itemType == item.itemType &&
                itemStatus == item.itemStatus &&
                Objects.equals(itemRank, item.itemRank) &&
                Objects.equals(itemProfile, item.itemProfile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, authors, itemType, itemStatus, itemRank, itemProfile);
    }
}
