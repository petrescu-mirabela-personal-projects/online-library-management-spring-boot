package com.spring.boot.OnlineLibrarySpringBoot.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "item_profile", schema = "wantsome")
public class ItemProfile implements Serializable {

    //private static final long serialVersionUID = -6153286234568847019L;

    @Id
    @Column(name = "id")
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;    // id column is not generated in database - profile table

    @Column(name = "publisher")
    private String publisher;

    // registered year
    @Column(name = "registered_year")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date registeredYear;

    // print/published year
    @Column(name = "published_year")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date publishedYear;

    /*
    https://www.baeldung.com/jpa-one-to-one - Shared Primary Key in JPA
    https://vladmihalcea.com/jpa-hibernate-synchronize-bidirectional-entity-associations/

    >>>> @MapsId tells Hibernate to use the id column of profile as both primary key and foreign key.
    >>>> @MapsId because we want the child table row to share the Primary Key with its parent table row
         meaning that the Primary Key is also a Foreign Key back to the parent table record.
    >>>> Also, notice that the @Id column of the Address entity no longer uses the @GeneratedValue annotation.
    >>>> bi-directional one-to-one association to User
    >>>> By default @ManyToOne and @OneToOne associations use the FetchType.EAGER strategy which is bad for performance.
    */
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    // @JoinColumn - Customize the Primary Key column name when using @MapsId.
    // @JoinColumn(name = "id")
    private Item item;  // item_id column is automatically generated in database - profile table

    public ItemProfile() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Date getPublishedYear() {
        return publishedYear;
    }

    public void setPublishedYear(Date publishedYear) {
        this.publishedYear = publishedYear;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Date getRegisteredYear() {
        return registeredYear;
    }

    public void setRegisteredYear(Date registeredYear) {
        this.registeredYear = registeredYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemProfile that = (ItemProfile) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(publisher, that.publisher) &&
                Objects.equals(registeredYear, that.registeredYear) &&
                Objects.equals(publishedYear, that.publishedYear) &&
                Objects.equals(item, that.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, publisher, registeredYear, publishedYear, item);
    }
}
