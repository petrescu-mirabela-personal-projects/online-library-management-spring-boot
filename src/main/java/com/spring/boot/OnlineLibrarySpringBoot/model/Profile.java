package com.spring.boot.OnlineLibrarySpringBoot.model;

import com.spring.boot.OnlineLibrarySpringBoot.model.types.GenderType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "profile", schema = "wantsome")
public class Profile implements Serializable {

    private static final long serialVersionUID = 2L;

    // https://www.baeldung.com/jpa-one-to-one - Shared Primary Key in JPA
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;     // id column is not generated in database - profile table

    /*
    https://www.baeldung.com/jpa-one-to-one - Shared Primary Key in JPA
    https://vladmihalcea.com/jpa-hibernate-synchronize-bidirectional-entity-associations/

    >>>> @MapsId tells Hibernate to use the id column of profile as both primary key and foreign key.
    >>>> @MapsId because we want the child table row to share the Primary Key with its parent table row
         meaning that the Primary Key is also a Foreign Key back to the parent table record.
    >>>> Also, notice that the @Id column of the Address entity no longer uses the @GeneratedValue annotation.
    >>>> bi-directional one-to-one association to User
    >>>> By default @ManyToOne and @OneToOne associations use the FetchType.EAGER strategy which is bad for performance.
    */
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    // @JoinColumn - Customize the Primary Key column name when using @MapsId.
    // @JoinColumn(name = "id")
    private User user;  // user_id column is automatically generated in database - profile table

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;
    private String address;
    private String occupation;

    // PA will use the Enum.ordinal() value when persisting a given entity in the database
    // LINK: https://www.baeldung.com/jpa-persisting-enums-in-jpa
    @Enumerated(EnumType.STRING)
    private GenderType gender;

    @Column(name = "date_of_birth")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateOfBirth;

    /* wiki: https://en.wikipedia.org/wiki/Hibernate_(framework)
     The only strict requirement for a persistent class is a no-argument constructor, though not necessarily public.
     */
//    public Profile() {
//    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", occupation='" + occupation + '\'' +
                ", gender=" + gender +
                ", dateOfBirth=" + dateOfBirth;
    }

    /* wiki: https://en.wikipedia.org/wiki/Hibernate_(framework)
    Proper behavior in some applications also requires special attention to the equals() and hashCode() methods in the object classes.
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profile profile = (Profile) o;
        return Objects.equals(id, profile.id) &&
                Objects.equals(user, profile.user) &&
                Objects.equals(firstName, profile.firstName) &&
                Objects.equals(lastName, profile.lastName) &&
                Objects.equals(address, profile.address) &&
                Objects.equals(occupation, profile.occupation) &&
                gender == profile.gender &&
                Objects.equals(dateOfBirth, profile.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, firstName, lastName, address, occupation, gender, dateOfBirth);
    }
}
