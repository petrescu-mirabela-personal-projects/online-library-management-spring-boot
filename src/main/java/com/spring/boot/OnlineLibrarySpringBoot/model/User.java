package com.spring.boot.OnlineLibrarySpringBoot.model;

import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/*
For storing users, we will create a User entity that is mapped to a database table, with the following attributes
 */

@Entity
@Table(name = "user", schema = "wantsome")
public class User implements Serializable {

    // to learn: BasicTypeRegistry (maps Hibernate, Java and Sql types);
    // https://docs.jboss.org/hibernate/orm/5.3/userguide/html_single/Hibernate_User_Guide.html
    // to learn: PhysicalNamingStrategy;

    private static final long serialVersionUID = -9067986418908847019L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @Basic being implicitly implied (For basic type attributes, the implicit naming rule is that the column name is the same as the attribute name.)
    private Integer id;

    @NotEmpty
    @Column(nullable = false, unique = true)
    // @Basic being implicitly implied (For basic type attributes, the implicit naming rule is that the column name is the same as the attribute name.)
    private String username;

    @NotEmpty
    @Column(nullable = false)
    // @Basic being implicitly implied (For basic type attributes, the implicit naming rule is that the column name is the same as the attribute name.)
    private String password;

    // @Basic being implicitly implied (For basic type attributes, the implicit naming rule is that the column name is the same as the attribute name.)
    private String email;

    @Column(name = "date_created")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateCreated;

    /*
     https://www.baeldung.com/jpa-one-to-one - Shared Primary Key in JPA
     https://vladmihalcea.com/jpa-hibernate-synchronize-bidirectional-entity-associations/

     >>>> The profile @OneToOne association is marked with the mappedBy attribute which indicates that
          the Profile side is responsible for handling this bidirectional association.
     >>>> If a Child entity is dissociated from its Parent, the Child Foreign Key is set to NULL.
          If we want to have the Child row deleted as well, we have to use the orphan removal support.
      */
    @OneToOne(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY)
    // @Basic being implicitly implied (For basic type attributes, the implicit naming rule is that the column name is the same as the attribute name.)
    private Profile profile;

    /* wiki: https://en.wikipedia.org/wiki/Hibernate_(framework)
        https://www.baeldung.com/jpa-many-to-many
     Related objects can be configured to cascade operations from one object to the other.
     For example, a parent Album class object can be configured to cascade its save and delete operations to its child Track class objects.
     */
    @ManyToMany(
            //cascade = CascadeType.ALL,    // authority role will not be deleted
            fetch = FetchType.EAGER)
    // LAZY - will not work here because I want to take all users with all its children!!!!!
    @JoinTable(
            name = "user_authority",
            //schema = "wantsome",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "authority_id")})
    // @Basic being implicitly implied (For basic type attributes, the implicit naming rule is that the column name is the same as the attribute name.)
    private Set<Authority> authorities = new HashSet<>();

     /* wiki: https://en.wikipedia.org/wiki/Hibernate_(framework)
     The only strict requirement for a persistent class is a no-argument constructor, though not necessarily public.
     */
     public User() {
     }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Profile getProfile() {
        return profile;
    }

    /*
    https://vladmihalcea.com/jpa-hibernate-synchronize-bidirectional-entity-associations/

    >>>> The setProfile method is used for synchronizing both sides of this bidirectional association and
         is used both for adding and removing the associated child entity.
     */
    public void setProfile(Profile profile) {
        if (profile == null) {
            if (this.profile != null) {
                this.profile.setUser(null);
            }
        } else {
            profile.setUser(this);
        }
        this.profile = profile;
    }


    /* wiki: https://en.wikipedia.org/wiki/Hibernate_(framework)
     Proper behavior in some applications also requires special attention to the equals() and hashCode() methods in the object classes.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username) &&
                Objects.equals(password, user.password) &&
                Objects.equals(email, user.email) &&
                Objects.equals(dateCreated, user.dateCreated) &&
                Objects.equals(profile, user.profile) &&
                Objects.equals(authorities, user.authorities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, email, dateCreated, profile, authorities);
    }
}


