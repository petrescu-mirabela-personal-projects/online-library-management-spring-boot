package com.spring.boot.OnlineLibrarySpringBoot.model;

import com.spring.boot.OnlineLibrarySpringBoot.controllers.LoginController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

/*
What we need to do to store the user information in database:
--> Create a User entity to store user information  @Entity
--> Store the User in out database
--> Link out User entity with the built in classes in Spring Security
--> Link User with UserDetails interface
--> Link UserRepository with UserDetailsService interface
--> Integrate Database Auth in out configuration
 */

public class UserDetailsImpl implements UserDetails {

    private static final Logger log = LogManager.getLogger(LoginController.class);

    @Autowired
    private User user;

    public UserDetailsImpl(User user) {
        log.info("UserDetailsImpl() user : {}", user.getUsername());
        this.user = user;
    }

//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        log.info("getAuthorities() message");
//
//        List<GrantedAuthority> authorities = new ArrayList<>();
//        authorities.add(new SimpleGrantedAuthority(String.valueOf(user.getUserType())));
//        return authorities;
//    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        log.info("getAuthorities() user authority : {}", user.getAuthorities());

        // extract list of roles
        return user.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(String.valueOf(authority.getName())))
                .collect(Collectors.toList());
    }

    public Integer getId() {
        return user.getId();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUserDetails() {
        return user;
    }
}
