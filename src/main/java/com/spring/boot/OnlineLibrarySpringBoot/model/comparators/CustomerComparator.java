package com.spring.boot.OnlineLibrarySpringBoot.model.comparators;


import com.spring.boot.OnlineLibrarySpringBoot.model.Customer;

import java.util.Comparator;

/*
LINK: https://howtodoinjava.com/sort/groupby-sort-multiple-comparators/
 */

public class CustomerComparator {

    public static class CustomerComparatorByCnp implements Comparator<Customer> {

        @Override
        public int compare(Customer o1, Customer o2) {
            return o1.getCnp().toLowerCase().compareTo(o2.getCnp().toLowerCase());
        }
    }

    public static class CustomerComparatorByFirstName implements Comparator<Customer> {

        @Override
        public int compare(Customer o1, Customer o2) {
            return o1.getFirstName().toLowerCase().compareTo(o2.getFirstName().toLowerCase());
        }
    }


}
