package com.spring.boot.OnlineLibrarySpringBoot.model.comparators;

import java.util.Comparator;
import java.util.List;

/**
 * it shall compare by multiple criteria => multiple comparators
 * the class has a generic type T
 * LINK: https://howtodoinjava.com/sort/groupby-sort-multiple-comparators/
 */
public class GroupBySorted<T> implements Comparator<T> {

    private List<Comparator<T>> listComparators;

//    public GroupBySorted(Comparator<T>... comparators) {
//        this.listComparators = Arrays.asList(comparators);
//    }

    public GroupBySorted(List<Comparator<T>> listComparators) {
        this.listComparators = listComparators;
    }

    public int compare(T empOne, T empTwo) {
        for (Comparator<T> comparator : listComparators) {
            int result = comparator.compare(empOne, empTwo);
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }
}
