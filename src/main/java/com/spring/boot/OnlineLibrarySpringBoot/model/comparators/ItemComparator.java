package com.spring.boot.OnlineLibrarySpringBoot.model.comparators;



/*
LINK: https://howtodoinjava.com/sort/groupby-sort-multiple-comparators/
 */

import com.spring.boot.OnlineLibrarySpringBoot.model.Item;

import java.util.Comparator;

public class ItemComparator {

    public static class ItemComparatorByTitle implements Comparator<Item> {

        @Override
        public int compare(Item o1, Item o2) {
            return o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
        }
    }

}
