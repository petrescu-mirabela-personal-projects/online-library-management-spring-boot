package com.spring.boot.OnlineLibrarySpringBoot.model.comparators;

import com.spring.boot.OnlineLibrarySpringBoot.model.User;

import java.util.Comparator;

/*
LINK: https://howtodoinjava.com/sort/groupby-sort-multiple-comparators/
 */

public class UserComparator {

    public static class UserComparatorByUserName implements Comparator<User> {

        @Override
        public int compare(User o1, User o2) {
            return o1.getUsername().toLowerCase().compareTo(o2.getUsername().toLowerCase());
        }
    }

    public static class UserComparatorByEmail implements Comparator<User> {

        @Override
        public int compare(User o1, User o2) {
            return o1.getEmail().toLowerCase().compareTo(o2.getEmail().toLowerCase());
        }
    }


    public static class UserComparatorByUserType implements Comparator<User> {

        @Override
        public int compare(User o1, User o2) {
            return String.valueOf(
                    o1.getAuthorities().stream().findFirst())
                    .compareTo(String.valueOf(o2.getAuthorities().stream().findFirst()));
        }
    }


    public static class UserComparatorByDateCreated implements Comparator<User> {

        @Override
        public int compare(User o1, User o2) {
            return o1.getDateCreated().compareTo(o2.getDateCreated());
        }
    }

}
