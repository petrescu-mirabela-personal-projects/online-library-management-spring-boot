package com.spring.boot.OnlineLibrarySpringBoot.model.types;

import com.spring.boot.OnlineLibrarySpringBoot.model.Authority;

import java.util.function.Predicate;

public enum AuthorityType {
    ROLE_ADMIN,
    ROLE_USER;
}
