package com.spring.boot.OnlineLibrarySpringBoot.model.types;

public enum GenderType {
    M("MALE"),
    F("FEMALE");

    private final String name;

    private GenderType(String name) {
        this.name = name;
    }

    /**
     * @return The string representation of this element in the enumeration.
     */
    public String getName() {
        return this.name;
    }

}
