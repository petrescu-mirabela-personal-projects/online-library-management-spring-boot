package com.spring.boot.OnlineLibrarySpringBoot.model.types;

public enum ItemStatus {
    AVAILABLE,
    LENT,
    LOST,
    DESTROYED,
    UNAVAILABLE
}
