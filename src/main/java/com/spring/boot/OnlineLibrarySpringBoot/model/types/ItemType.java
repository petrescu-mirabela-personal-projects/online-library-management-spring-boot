package com.spring.boot.OnlineLibrarySpringBoot.model.types;

public enum ItemType {
    BOOK,
    MAGAZINE,
    ARTICLE,
    DIGITAL_STORAGE_MEDIA
}
