package com.spring.boot.OnlineLibrarySpringBoot.repository;

import com.spring.boot.OnlineLibrarySpringBoot.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {

    Author findByName(String name);

    Set<Author> findByNameContaining(String name);
}
