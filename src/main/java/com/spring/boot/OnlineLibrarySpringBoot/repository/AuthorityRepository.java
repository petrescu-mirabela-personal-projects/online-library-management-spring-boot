package com.spring.boot.OnlineLibrarySpringBoot.repository;

import com.spring.boot.OnlineLibrarySpringBoot.model.Authority;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.AuthorityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Integer> {

    Authority findByName(AuthorityType type);
}
