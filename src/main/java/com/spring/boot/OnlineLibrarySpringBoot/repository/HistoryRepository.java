package com.spring.boot.OnlineLibrarySpringBoot.repository;

import com.spring.boot.OnlineLibrarySpringBoot.model.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
For the purpose of retrieving a user associated with a username,
we will create a DAO class using Spring Data by extending the JpaRepository interface
 */

@Repository
public interface HistoryRepository extends JpaRepository<History, Integer> {

}
