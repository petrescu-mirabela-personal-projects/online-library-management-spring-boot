package com.spring.boot.OnlineLibrarySpringBoot.repository;


import com.spring.boot.OnlineLibrarySpringBoot.model.ItemProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ItemProfileRepository extends JpaRepository<ItemProfile, Integer> {


}
