package com.spring.boot.OnlineLibrarySpringBoot.repository;

import com.spring.boot.OnlineLibrarySpringBoot.model.Author;
import com.spring.boot.OnlineLibrarySpringBoot.model.Item;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.ItemType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

    // multiple items with the same title can be registered
    List<Item> findByTitle(String title);

    List<Item> findByItemType(ItemType type);

    //Item findByAuthor(String author);

    // find the items with the title starting with title text
    List<Item> findByTitleContaining(String title);

    @Query(value = "SELECT * FROM item WHERE id IN (SELECT item_id FROM item_author WHERE author_id = (SELECT name FROM author WHERE name LIKE %?1%) )", nativeQuery = true)
    List<Item> findByAuthorContaining(String author);

    @Query(value = "SELECT * FROM item WHERE item_rank_id IN (SELECT id FROM item_rank WHERE name LIKE %?1%)", nativeQuery = true)
    List<Item> findByRankContaining(String rank);

    @Query(value = "SELECT * FROM item WHERE id IN (SELECT item_id FROM item_profile WHERE registered_year <= ?1)", nativeQuery = true)
    List<Item> findBeforeRegisteredYear(Date registeredYear);

    @Query(value = "SELECT * FROM item WHERE id IN (SELECT item_id FROM item_profile WHERE published_year <= ?1)", nativeQuery = true)
    List<Item> findBeforePublishedYear(Date publishedYear);


//    // LINK: https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#reference
//    @Query(value = "SELECT authors FROM item i where authors in (select user_id FROM user_authority where authority_id=(select id FROM authority where name=?1))", nativeQuery = true)
//    List<Author> getAllAuthors(String authorityType);
}
