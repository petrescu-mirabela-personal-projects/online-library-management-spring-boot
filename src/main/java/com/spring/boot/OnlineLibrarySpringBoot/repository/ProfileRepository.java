package com.spring.boot.OnlineLibrarySpringBoot.repository;

import com.spring.boot.OnlineLibrarySpringBoot.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileRepository extends JpaRepository<Profile, Integer> {
}
