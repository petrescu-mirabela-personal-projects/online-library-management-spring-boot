package com.spring.boot.OnlineLibrarySpringBoot.repository;

import com.spring.boot.OnlineLibrarySpringBoot.model.Authority;
import com.spring.boot.OnlineLibrarySpringBoot.model.User;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.AuthorityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
For the purpose of retrieving a user associated with a username,
we will create a DAO class using Spring Data by extending the JpaRepository interface
 */

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);

    List<User> findByUsernameContaining(String username);

    // LINK: https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#reference
    @Query(value = "SELECT * FROM user u WHERE id IN (select user_id FROM user_authority WHERE authority_id=(SELECT id FROM authority WHERE name=?1))", nativeQuery = true)
    List<User> getAllAdmins(String authorityType);

    //@Query(value = "SELECT * FROM user u where username=?1 and password=?2))", nativeQuery = true)
    //User getUserByUsernameAndPassword(String username, String password);

    User findByUsernameAndPassword(String username, String password);

}
