package com.spring.boot.OnlineLibrarySpringBoot.security;


import com.spring.boot.OnlineLibrarySpringBoot.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

//Link:https://www.youtube.com/watch?v=hF-iMHpl970&list=PLVApX3evDwJ1d0lKKHssPQvzv2Ao3e__Q&index=11
// https://www.baeldung.com/spring_redirect_after_login

/*
 When we want to change the default configuration, we can customize the WebSecurityConfigurerAdapter

 @Bean --> all the beans are loaded into the SpringContext,
            so all the injected components in the DaoAuthenticationProvider are correctly resolved.
 */

@Configuration
@EnableWebSecurity//(debug = true)
@ComponentScan(basePackages = "com.spring.boot.OnlineLibrarySpringBoot.services")
@ComponentScan(basePackages = "com.spring.boot.OnlineLibrarySpringBoot.security")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserDetailsService userDetailsService;

    // in memory authentication
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.
//                inMemoryAuthentication()
//                .withUser("mira")
//                .password(passwordEncoder().encode("bela"))
//                .roles("ADMIN")
//                .and()
//                .withUser("marius")
//                .password(passwordEncoder().encode("1234"))
//                .roles("LIBRARIAN");
//    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.
                authenticationProvider(authenticationProvider());
    }

    // any user regardless their roles can access everything
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .anyRequest().authenticated()
//                .and()
//                .httpBasic();
//    }

    // role based authentication
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/login", "/fragments", "/index").permitAll()
                .antMatchers("/login/create_user_account").permitAll()
                .antMatchers("/resources/**", "/css/**", "/js/**").permitAll()
                .antMatchers("/adminView/**").hasRole("ADMIN")
                .antMatchers("/customerView/**").hasRole("USER")
                .antMatchers("/itemView/**").hasRole("USER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").usernameParameter("username").passwordParameter("password").permitAll()
                .loginProcessingUrl("/doLogin")
                .successForwardUrl("/postLogin")
                //.successHandler(myAuthenticationSuccessHandler())
                .failureUrl("/loginFailed")
                .permitAll()
                .and()
                .rememberMe().tokenValiditySeconds(2592000).rememberMeParameter("remember-me")
                .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .logoutSuccessUrl("/login")
                .permitAll();
    }

    /*
    Database Authentication - Configure Database Provider
    - Integrate UserDetailsImpl and UserDetailsServiceImpl into the Spring Security configuration mechanism.
    - requires user details which are provided in UserDetailService
     */
    @Bean
    DaoAuthenticationProvider authenticationProvider() {
        log.info("authenticationProvider() method call");

        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();

        log.info("authenticationProvider() before ==> " + daoAuthenticationProvider.toString());

        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);

        log.info("authenticationProvider() after ==> " + daoAuthenticationProvider.toString());

        return daoAuthenticationProvider;
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(10);

        log.info("passwordEncoder() ==> " + passwordEncoder);

        return passwordEncoder;
    }

    @Bean
    public AuthenticationSuccessHandler myAuthenticationSuccessHandler() {
        return new CustomAuthenticationSuccessHandler();
    }
}
