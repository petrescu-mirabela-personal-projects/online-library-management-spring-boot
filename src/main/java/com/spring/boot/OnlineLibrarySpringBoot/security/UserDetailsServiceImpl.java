package com.spring.boot.OnlineLibrarySpringBoot.security;

import com.spring.boot.OnlineLibrarySpringBoot.model.User;
import com.spring.boot.OnlineLibrarySpringBoot.model.UserDetailsImpl;
import com.spring.boot.OnlineLibrarySpringBoot.repository.UserRepository;
import com.spring.boot.OnlineLibrarySpringBoot.services.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

// TO PROVIDE OUR OWN USER SERVICE, WE WILL NEED TO IMPLEMENT THE UserDetailsService INTERFACE

/*
1) !!!!  Implementation that retrieves user details from UserDetailsService

2) The UserDetailsService interface is used to retrieve user-related data.
It has one method named loadUserByUsername() which can be overridden to customize the process of finding the user
It is used by the DaoAuthenticationProvider to load details about the user during authentication.
 */

@Service
// => then the app will automatically detect it during component-scan and will create a bean out of this class
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    /*
    - retrieve the User object using the DAO, and
    - if it exists, wrap it into a UserDetailsImpl object, which implements UserDetails, and
    - returns it
     */
    //@Transactional//(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        log.info("UserDetailsServiceImpl ==> database username : {}", user.getUsername());
        log.info("UserDetailsServiceImpl ==> database password : {}", user.getPassword());

        if (user == null) {
            log.info("UserDetailsServiceImpl class ==> User not found");
            throw new UsernameNotFoundException("User not found");
        }
        log.info("UserDetailsServiceImpl ==> username : {}", username);

        return new UserDetailsImpl(user);
    }
}
