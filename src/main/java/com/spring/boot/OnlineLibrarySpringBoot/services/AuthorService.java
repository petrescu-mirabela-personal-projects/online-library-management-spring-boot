package com.spring.boot.OnlineLibrarySpringBoot.services;

import com.spring.boot.OnlineLibrarySpringBoot.model.Author;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface AuthorService {

    Author addAuthor(Author author);

    List<Author> getAllAuthors();

    Author findByName(String name);

    Set<Author> findByNameContaining(String name);

    Author getAuthorById(Integer id);

    void updateAuthor(Integer id, Author author);

    void deleteAuthor(Integer id);

    //public List<Author> sortAuthors(String[] authorComparator);
}
