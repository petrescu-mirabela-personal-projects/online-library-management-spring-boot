package com.spring.boot.OnlineLibrarySpringBoot.services;

import com.spring.boot.OnlineLibrarySpringBoot.model.Authority;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AuthorityService {

    //Authority addAuthority(Authority authority);

    Authority getAuthorityById(Integer id);

    Authority updateAuthority(Authority authority);

    //void deleteAuthority(Integer id);

    List<Authority> getAllAuthorities();

}
