package com.spring.boot.OnlineLibrarySpringBoot.services;

import com.spring.boot.OnlineLibrarySpringBoot.model.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {

    Iterable<Customer> getAllCustomers();

    void addCustomer(Customer customer);

    void updateCustomer(Integer customerId, Customer customer);

    Customer getCustomerById(Integer customerId);

    void deleteCustomer(Integer customerId);

    List<Customer> findByCnpContaining(String cnp);

    List<Customer> findByFirstName(String firstName);
}
