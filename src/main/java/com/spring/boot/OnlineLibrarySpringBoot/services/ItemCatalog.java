package com.spring.boot.OnlineLibrarySpringBoot.services;

import com.spring.boot.OnlineLibrarySpringBoot.model.Item;
import com.spring.boot.OnlineLibrarySpringBoot.model.ItemRank;
import com.spring.boot.OnlineLibrarySpringBoot.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

public class ItemCatalog implements ItemSearch {

    @Autowired
    ItemService itemService;

    @Autowired
    ItemRankService itemRankService;

    @Override
    public List<Item> searchByTitle(String title) {
        return itemService.findByTitleContaining(title);
    }

    @Override
    public List<Item> searchByAuthor(String author) {
        return itemService.findByAuthorContaining(author);
    }

    @Override
    public List<Item> searchByRank(String rank) {
        return itemService.findByRankContaining(rank);
    }

    @Override
    public List<Item> searchByRegisteredYear(Date year) {
        return null;
    }

    @Override
    public List<Item> searchByPublishedYear(Date year) {
        return null;
    }
}
