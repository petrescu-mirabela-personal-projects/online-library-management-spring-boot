package com.spring.boot.OnlineLibrarySpringBoot.services;

import com.spring.boot.OnlineLibrarySpringBoot.model.ItemRank;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ItemRankService {

    List<ItemRank> getAllItemRank();

    ItemRank findByName(String name);

    void deleteItemRank(ItemRank itemRank);

    ItemRank findById(Integer id);

    void addItemRank(ItemRank itemRank);

    ItemRank updateItemRank(ItemRank itemRank);

}
