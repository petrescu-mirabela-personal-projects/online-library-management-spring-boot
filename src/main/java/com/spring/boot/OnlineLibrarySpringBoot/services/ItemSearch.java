package com.spring.boot.OnlineLibrarySpringBoot.services;

import com.spring.boot.OnlineLibrarySpringBoot.model.Item;

import java.util.Date;
import java.util.List;

public interface ItemSearch {

    List<Item> searchByTitle(String title);

    List<Item> searchByAuthor(String author);

    List<Item> searchByRank(String rank);

    List<Item> searchByRegisteredYear(Date year);

    List<Item> searchByPublishedYear(Date year);
}
