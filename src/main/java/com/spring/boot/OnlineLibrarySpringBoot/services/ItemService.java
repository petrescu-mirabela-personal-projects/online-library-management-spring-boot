package com.spring.boot.OnlineLibrarySpringBoot.services;

import com.spring.boot.OnlineLibrarySpringBoot.model.Item;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.ItemType;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public interface ItemService {

    Iterable<Item> getAllItems();

    void addItem(Item item);

    Item updateItem(Integer itemId, Item item);

    Item getItemById(Integer id);

    void deleteItem(Integer itemId);

    public List<Item> sortItems(String[] itemComparator);

    List<Item> findByTitleContaining(String title);

    List<Item> findByItemType(ItemType type);

    List<Item> findByAuthorContaining(String author);

    List<Item> findByRankContaining(String rank);

    List<Item> findBeforeRegisteredYear(Date registeredYear);

    List<Item> findBeforePublishedYear(Date publishedYear);
}
