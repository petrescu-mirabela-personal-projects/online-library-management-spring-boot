package com.spring.boot.OnlineLibrarySpringBoot.services;

import com.spring.boot.OnlineLibrarySpringBoot.model.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProfileService {

    Profile getProfileById(Integer id);

    void updateProfile(Integer id, Profile profile);

    void addProfile(Profile profile);

}
