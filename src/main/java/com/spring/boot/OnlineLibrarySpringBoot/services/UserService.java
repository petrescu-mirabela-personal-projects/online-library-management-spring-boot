package com.spring.boot.OnlineLibrarySpringBoot.services;

import com.spring.boot.OnlineLibrarySpringBoot.model.Authority;
import com.spring.boot.OnlineLibrarySpringBoot.model.Profile;
import com.spring.boot.OnlineLibrarySpringBoot.model.User;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.AuthorityType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    User addUser(User user);

    List<User> getAllUsers();

    User findByUsername(String username);

    User getUserByUsernameAndPassword(String username, String password);

    List<User> findByUsernameContaining(String username);

    User getUserById(Integer id);

    List<User> getAllAdmins();

    void updateUser(Integer id, User user);

    void deleteUser(Integer userId);

    //User.UserType getUserTypeLogin(String username, String password);

    Profile getProfileByUserId(Integer userId);

    User getLastUser();

    public List<User> sortUsers(String[] userComparator) throws NullPointerException;


}
