package com.spring.boot.OnlineLibrarySpringBoot.services.impl;

import com.spring.boot.OnlineLibrarySpringBoot.model.Author;
import com.spring.boot.OnlineLibrarySpringBoot.repository.AuthorRepository;
import com.spring.boot.OnlineLibrarySpringBoot.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    AuthorRepository authorRepository;

    @Override
    public Author addAuthor(Author author) {
        return authorRepository.save(author);
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    @Override
    public Author findByName(String name) {
        return authorRepository.findByName(name);
    }

    @Override
    public Set<Author> findByNameContaining(String name) {
        return authorRepository.findByNameContaining(name);
    }

    @Override
    public Author getAuthorById(Integer id) {
        return authorRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public void updateAuthor(Integer id, Author author) {
        if (authorRepository.findById(id).isPresent()) {
            authorRepository.save(author);
        }
    }

    @Override
    public void deleteAuthor(Integer id) {
        authorRepository.deleteById(id);
    }

//    @Override
//    public List<Author> sortAuthors(String[] authorComparator) {
//        List<Comparator<Author>> listComparators = new ArrayList<>();
//
//        // If The sort button is pressed AND
//        // there is at least one sort option selected in the browser
//        if (authorComparator != null) {
//            for (String report : authorComparator) {
//                switch (report) {
//                    case "userCompByName":
//                        listComparators.add(new AuthorComparator.AuthorComparatorByName());
//                        break;
//                }
//            }
//        }
//
//        // Sort the users applying the group of comparators
//        List<Author> users = authorRepository.findAll();
//        users.sort(new GroupBySorted(listComparators));
//        return users;
//    }
}
