package com.spring.boot.OnlineLibrarySpringBoot.services.impl;

import com.spring.boot.OnlineLibrarySpringBoot.model.Authority;
import com.spring.boot.OnlineLibrarySpringBoot.repository.AuthorityRepository;
import com.spring.boot.OnlineLibrarySpringBoot.services.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    @Autowired
    AuthorityRepository authorityRepository;

    @Override
    public Authority getAuthorityById(Integer id) {
        return authorityRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Authority updateAuthority(Authority authority) {
        return authorityRepository.save(authority);
    }

    @Override
    public List<Authority> getAllAuthorities() {
        return authorityRepository.findAll();
    }


}
