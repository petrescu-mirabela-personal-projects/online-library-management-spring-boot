package com.spring.boot.OnlineLibrarySpringBoot.services.impl;

import com.spring.boot.OnlineLibrarySpringBoot.model.Customer;
import com.spring.boot.OnlineLibrarySpringBoot.repository.CustomerRepository;
import com.spring.boot.OnlineLibrarySpringBoot.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Iterable<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public void addCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void updateCustomer(Integer customerId, Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.findById(customerId).orElseThrow(IllegalAccessError::new);
    }

    @Override
    public void deleteCustomer(Integer customerId) {
        customerRepository.deleteById(customerId);
    }

    @Override
    public List<Customer> findByCnpContaining(String cnp) {
        return customerRepository.findByCnpContaining(cnp);
    }

    @Override
    public List<Customer> findByFirstName(String firstName) {
        return customerRepository.findByFirstName(firstName);
    }
}
