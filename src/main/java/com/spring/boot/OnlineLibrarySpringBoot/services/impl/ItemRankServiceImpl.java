package com.spring.boot.OnlineLibrarySpringBoot.services.impl;

import com.spring.boot.OnlineLibrarySpringBoot.model.ItemRank;
import com.spring.boot.OnlineLibrarySpringBoot.repository.ItemRankRepository;
import com.spring.boot.OnlineLibrarySpringBoot.services.ItemRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemRankServiceImpl implements ItemRankService {

    @Autowired
    ItemRankRepository itemRankRepository;

    @Override
    public List<ItemRank> getAllItemRank() {
        return itemRankRepository.findAll();
    }

    @Override
    public ItemRank findByName(String name) {
        return itemRankRepository.findByName(name);
    }

    @Override
    public void deleteItemRank(ItemRank itemRank) {
        itemRankRepository.delete(itemRank);
    }

    @Override
    public ItemRank findById(Integer id) {
        return itemRankRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public void addItemRank(ItemRank itemRank) {
        itemRankRepository.save(itemRank);
    }

    @Override
    public ItemRank updateItemRank(ItemRank itemRank) {
        return itemRankRepository.save(itemRank);
    }
}
