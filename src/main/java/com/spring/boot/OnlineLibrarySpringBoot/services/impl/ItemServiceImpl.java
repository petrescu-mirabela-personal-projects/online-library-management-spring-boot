package com.spring.boot.OnlineLibrarySpringBoot.services.impl;

import com.spring.boot.OnlineLibrarySpringBoot.model.Item;
import com.spring.boot.OnlineLibrarySpringBoot.model.comparators.GroupBySorted;
import com.spring.boot.OnlineLibrarySpringBoot.model.comparators.ItemComparator;
import com.spring.boot.OnlineLibrarySpringBoot.model.types.ItemType;
import com.spring.boot.OnlineLibrarySpringBoot.repository.ItemRepository;
import com.spring.boot.OnlineLibrarySpringBoot.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    ItemRepository itemRepository;

    @Override
    public List<Item> getAllItems() {
        return itemRepository.findAll();
    }

    @Override
    public void addItem(Item item) {
        itemRepository.save(item);
    }

    @Override
    public Item updateItem(Integer id, Item item) {
        return itemRepository.save(item);
    }

    @Override
    public Item getItemById(Integer id) {
        return itemRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public void deleteItem(Integer id) {
        itemRepository.deleteById(id);
    }

    @Override
    public List<Item> sortItems(String[] itemComparator) {
        List<Comparator<Item>> listComparators = new ArrayList<>();

        // If The sort button is pressed AND
        // there is at least one sort option selected in the browser
        if (itemComparator != null) {
            for (String report : itemComparator) {
                switch (report) {
                    case "itemCompByTitle":
                        listComparators.add(new ItemComparator.ItemComparatorByTitle());
                        break;
                }
            }
        }

        // Sort the users applying the group of comparators
        List<Item> users = itemRepository.findAll();
        users.sort(new GroupBySorted<>(listComparators));
        return users;
    }

    @Override
    public List<Item> findByTitleContaining(String title) {
        return itemRepository.findByTitleContaining(title);
    }

    @Override
    public List<Item> findByItemType(ItemType type) {
        return itemRepository.findByItemType(type);
    }

    @Override
    public List<Item> findByAuthorContaining(String author) {
        return itemRepository.findByAuthorContaining(author);
    }

    @Override
    public List<Item> findByRankContaining(String rank) {
        return itemRepository.findByRankContaining(rank);
    }

    @Override
    public List<Item> findBeforeRegisteredYear(Date registeredYear) {
        return itemRepository.findBeforeRegisteredYear(registeredYear);
    }

    @Override
    public List<Item> findBeforePublishedYear(Date publishedYear) {
        return itemRepository.findBeforePublishedYear(publishedYear);
    }
}
