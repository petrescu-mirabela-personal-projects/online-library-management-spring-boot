package com.spring.boot.OnlineLibrarySpringBoot.services.impl;

import com.mysql.cj.log.ProfilerEvent;
import com.spring.boot.OnlineLibrarySpringBoot.model.Profile;
import com.spring.boot.OnlineLibrarySpringBoot.repository.ProfileRepository;
import com.spring.boot.OnlineLibrarySpringBoot.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    ProfileRepository profilerRepository;

    @Override
    public Profile getProfileById(Integer id) {
        return profilerRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public void updateProfile(Integer id, Profile profile) {
        if (profilerRepository.findById(id).isPresent()) {
            profilerRepository.save(profile);
        }
    }

    @Override
    public void addProfile(Profile profile) {
        profilerRepository.save(profile);
    }
}
