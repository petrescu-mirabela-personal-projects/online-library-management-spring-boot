package com.spring.boot.OnlineLibrarySpringBoot.services.impl;

import com.spring.boot.OnlineLibrarySpringBoot.model.Profile;
import com.spring.boot.OnlineLibrarySpringBoot.model.User;
import com.spring.boot.OnlineLibrarySpringBoot.model.comparators.GroupBySorted;
import com.spring.boot.OnlineLibrarySpringBoot.model.comparators.UserComparator;
import com.spring.boot.OnlineLibrarySpringBoot.repository.UserRepository;
import com.spring.boot.OnlineLibrarySpringBoot.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User addUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        System.out.println(">>>>>>>>>>>>> will print a proxy and not the class name" + userRepository.getClass());
        // OUTPUT: >>>>>>>>>>>>>class com.sun.proxy.$Proxy133
        return userRepository.findAll();
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        return userRepository.findByUsernameAndPassword(username, password);
    }

    @Override
    public List<User> findByUsernameContaining(String username) {
        return userRepository.findByUsernameContaining(username);
    }

    @Override
    public User getUserById(Integer id) {
        return userRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public List<User> getAllAdmins() {
        return userRepository.getAllAdmins("ROLE_ADMIN");
    }

    @Override
    public void updateUser(Integer id, User user) {
        if (userRepository.findById(id).isPresent()) {
            userRepository.save(user);
        }
    }

    @Override
    public void deleteUser(Integer userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public Profile getProfileByUserId(Integer userId) {
        return getUserById(userId).getProfile();
    }

    @Override
    public User getLastUser() {
        return userRepository.findAll().stream().max(Comparator.comparing(User::getId)).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public List<User> sortUsers(String[] userComparator) throws IllegalArgumentException {
        List<Comparator<User>> listComparators = new ArrayList<>();

        // If The sort button is pressed AND
        // there is at least one sort option selected in the browser
        if (userComparator != null) {
            for (String report : userComparator) {
                switch (report) {
                    case "userCompByRole":
                        listComparators.add(new UserComparator.UserComparatorByUserType());
                        break;
                    case "userCompByUsername":
                        listComparators.add(new UserComparator.UserComparatorByUserName());
                        break;
                    case "userCompByEmail":
                        listComparators.add(new UserComparator.UserComparatorByEmail());
                        break;
                    case "userCompByDateCreated":
                        listComparators.add(new UserComparator.UserComparatorByDateCreated());
                        break;
                }
            }
        }

        // Sort the users applying the group of comparators
        List<User> users = userRepository.findAll();
        users.sort(new GroupBySorted<>(listComparators));
        return users;
    }
}
