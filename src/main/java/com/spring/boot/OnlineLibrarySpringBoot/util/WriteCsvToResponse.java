package com.spring.boot.OnlineLibrarySpringBoot.util;

import com.spring.boot.OnlineLibrarySpringBoot.model.Authority;
import com.spring.boot.OnlineLibrarySpringBoot.model.User;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

// LINK: http://zetcode.com/articles/springbootcsv/

public class WriteCsvToResponse <T>{

    /**
     * CREATE A STRING FROM AN OBJECT ADDING , BETWEEN OBJECT FIELDS
     */
    public Function<User, String> mapToString =
        obj -> new StringBuilder()
                .append(obj.getId()).append(",")
                .append(obj.getUsername()).append(",")
                .append(obj.getPassword()).append(",")
                .append(obj.getEmail()).append(",")
                .append(obj.getAuthorities()
                        .stream()
                        .map(Authority::getName)
                        .toString())
                .toString();

    /**
     * RETURN A STRING FROM AN OBJECT
     * - each object field separated by ","
     * - each object list separated by "\n"
     */
    public static <T> String getStringFromObject(List<T> obj, Function<T, String> mapToString) {
        return obj.stream()
                .map(mapToString::apply)
                .collect(Collectors.joining("\n"));
    }

    /**
     * WRITE STRING INFO TO A STREAM
     */
    public void writeToFileInfo(String text, OutputStream out) throws IOException {
        char[] cArray = text.toCharArray();
        for (char textChar : cArray) {
            out.write(textChar);
        }
    }



    private static final Logger LOGGER = LoggerFactory.getLogger(WriteCsvToResponse.class);

    // generic function to write into a csv file all the information from database table
    public void writeValuesListToCsv(PrintWriter writer, List<T> valuesList)  {

        try {

//            ColumnPositionMappingStrategy mapStrategy
//                    = new ColumnPositionMappingStrategy();
//            mapStrategy.setType(User.class);
//            String[] CSV_HEADER = new String[]{"id", "username", "password", "realName", "email", "userType"};
//            mapStrategy.setColumnMapping(CSV_HEADER);



            //create a csv writer
            StatefulBeanToCsv<T> stbcsv = new StatefulBeanToCsvBuilder<T>(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                    //.withMappingStrategy(mapStrategy)
                    .withOrderedResults(true)
                    .build();

            //write all users to csv file
            stbcsv.write(valuesList);

        } catch (CsvException ex) {

            LOGGER.error("Error mapping Bean to CSV", ex);
        }
    }
}
