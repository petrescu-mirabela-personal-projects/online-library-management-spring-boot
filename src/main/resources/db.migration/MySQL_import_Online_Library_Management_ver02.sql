select * from user;
select * from user_authority;
select * from authority;
select * from profile;

select * from customer;

select * from customer;

INSERT INTO `authority`(`name`, `id`) VALUES ('ROLE_ADMIN', 1);
INSERT INTO `authority`(`name`, `id`) VALUES ('ROLE_USER', 2);

INSERT INTO `profile`(`user_id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (1,"Iowa,USA",'1993-11-15 22:14:54',"Yogen","M","Rai","Engineer");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`) VALUES (1,'ironman','$2a$10$jXlure/BaO7K9WSQ8AMiOu3Ih3Am3kmmnVkWWHZEcQryZ8QPO3FgC','ironman@gmail.com','2015-11-15 22:14:54');

INSERT INTO `profile`(`user_id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (2,"Kathmandu,Nepal",'1992-11-15 22:14:54',"Rabi","M","Maharjan","Student");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`) VALUES (2,'rabi','$2a$10$0tFJKcOV/Io6I3vWs9/Tju8OySoyMTpGAyO0zaAOCswMbpfma0BSK','rabi@gmail.com','2015-10-15 22:14:54');

INSERT INTO `profile`(`user_id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (3,"Indonesia",'1992-11-15 22:14:54',"John","M","Rockie","Teacher");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`) VALUES (3,'rockie','$2a$10$WILylGNCM9SxeOKL2wKT9ukUlt3XEjeo1ULn/qB66duZokcOGT.G.','rockie92@gmail.com','2015-10-16 22:14:54');

INSERT INTO `profile`(`user_id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (4,"Mumbai, India",'1990-11-15 22:14:54',"Rita","F","Khaling","Engineer");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`) VALUES (4,'rita','$2a$10$ANuESyLFe1PgzWQkq1Y1IeoWWYEtDa/47bmmig1di6CVZOwe7MGh6','khaling.rita.92@gmail.com','2015-10-12 22:14:54');

INSERT INTO `profile`(`user_id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (5,"NULL",'1992-11-15 22:14:54',"NULL","M","NULL","NULL");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`) VALUES (5,'manish','$2a$10$jr7iN4WedV/gW7FD.jov.OrpfhBpxsPMiss3R4ZmGjNqLJgR.2ZRC','manish@gmail.com','2015-10-13 22:14:54');

INSERT INTO `profile`(`user_id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (6,"Romania",'2019-11-15 22:14:54',"Mirabela","F","Petrescu","Programmer");
INSERT INTO `user` (`id`, `username`, `password`, `email`,`date_created`) VALUES (6,'mira','$2a$10$iDmqb8/bUeIjkaIamWPvZek8tZY0MdYPfvRGy6qILX52jYStSVtn.','manish@gmail.com','2015-10-13 22:14:54');

INSERT INTO `profile`(`user_id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (7,"Romania",'2019-11-15 22:14:54',"Razvan","M","Popa","Programmer");
INSERT INTO `user` (`username`, `password`, `email`,`date_created`) VALUES ('popa','$2a$10$iDmqb8/bUeIjkaIamWPvZek8tZY0MdYPfvRGy6qILX52jYStSVtn.','popa@gmail.com','2015-10-13 22:14:54');

INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (1, 1);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (2, 2);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (2, 3);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (1, 4);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (2, 5);
INSERT INTO `user_authority`(`authority_id`, `user_id`) VALUES (1, 6);

update user set password = "$2a$10$nrFINzc4dTpE6vV8zwyR6eN9tQkoqoQd8o2aC1rHkuvrn9Fh1OIai" where id = 6;	
INSERT INTO `profile`(`user_id`, `address`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (11,"Iasi, Romania",'1993-11-15 22:14:54',"Marius","M","Petrescu","Engineer");
delete from user where id = 18;
delete from profile where user_id = 11;

SELECT * FROM user u where id in (select user_id FROM user_authority where authority_id=(select id FROM authority where name="ROLE_ADMIN"));

drop table item;
drop table item_author;
drop table customer;
drop table history;
drop table author;

select * from item;

INSERT INTO customer (`id`, `address`, `cnp`, `date_created`, `date_of_birth`, `first_name`, `gender`, `last_name`, `occupation`) VALUES (1, "Iasi", "12345678910", '2020-01-10', '1993-11-15', "Petrescu", "F", "Mirabela", "SW");