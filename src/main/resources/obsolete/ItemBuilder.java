//package com.spring.boot.OnlineLibrarySpringBoot.model;
//
//import com.spring.boot.OnlineLibrarySpringBoot.model.types.ItemStatus;
//import com.spring.boot.OnlineLibrarySpringBoot.model.types.ItemType;
//import org.springframework.format.annotation.DateTimeFormat;
//
//import javax.persistence.*;
//import java.util.Date;
//import java.util.Objects;
//import java.util.Set;
//
//@Entity
//@Table(name = "item", schema = "wantsome")
//public class ItemBuilder {
//
//    //////////// BUILDER PATTERN ////////////////////
//
//    // Class has many fields, some mandatory and some optional
//    /*
//    MANDATORY ATTRIBUTES
//     */
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//    private String title;
//
//    @ManyToMany
//    private Set<Author> authors;
//
//    // JPA will use the Enum.ordinal() value when persisting a given entity in the database
//    // LINK: https://www.baeldung.com/jpa-persisting-enums-in-jpa
//    @Column(name = "item_type")
//    @Enumerated(EnumType.STRING)
//    private ItemType itemType;
//
//    @Column(name = "item_status")
//    @Enumerated(EnumType.STRING)
//    private ItemStatus itemStatus;
//
//    /*
//    OPTIONAL ATTRIBUTES
//     */
//    // where the item is stored in the library
//    private String rank;
//
//    @Column(name = "publishing_house")
//    private String publishingHouse;
//
//    @Column(name = "record_store")
//    private String recordStore;
//
//    // print/published year
//    @Column(name = "published_year")
//    @Temporal(TemporalType.DATE)
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    private Date publishedYear;
//
//    public Item() {
//    }
//
//    // Has a SINGLE CONSTRUCTOR, PRIVATE, with params for ALL fields! (mandatory+optional)
//    //   -> will be called only by Builder, not from outside
//    private Item(Long id, String title, ItemType itemType, ItemStatus itemStatus, Set<Author> authors, String rank, String publishingHouse, String recordStore, Date publishedYear) {
//        this.id = id;
//        this.title = title;
//        this.itemType = itemType;
//        this.itemStatus = itemStatus;
//        this.authors = authors;
//        this.rank = rank;
//        this.publishingHouse = publishingHouse;
//        this.recordStore = recordStore;
//        this.publishedYear = publishedYear;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public ItemType getItemType() {
//        return itemType;
//    }
//
//    public ItemStatus getItemStatus() {
//        return itemStatus;
//    }
//
//    public Set<Author> getAuthors() {
//        return authors;
//    }
//
//    public String getRank() {
//        return rank;
//    }
//
//    public String getPublishingHouse() {
//        return publishingHouse;
//    }
//
//    public String getRecordStore() {
//        return recordStore;
//    }
//
//    public Date getYear() {
//        return publishedYear;
//    }
//
//    //A SEPARATE BUILDER CLASS for our target class (Item)
//    //NEEDS to be a STATIC INNER CLASS of it! (in order to have access to its private constructor)
//        public static class Builder{
//
//        //Builder has COPIES of ALL FIELDS of target class (Item)
//
//        /*
//        MANDATORY ATTRIBUTES
//         */
//        private Long id;
//        private String title;
//        private ItemType itemType;
//        private ItemStatus itemStatus;
//        private Set<Author> authors;
//
//        /*
//        OPTIONAL ATTRIBUTES
//         */
//        // where the item is stored in the library
//        private String rank;
//        private String publishingHouse;
//        private String recordStore;
//        // print/published year
//        private Date publishedYear;
//
//        // Has a SINGLE CONSTRUCTOR, PUBLIC, with params for all MANDATORY fields
//
//
//        public Builder(Long id, String title, ItemType itemType, ItemStatus itemStatus, Set<Author> authors) {
//            this.id = id;
//            this.title = title;
//            this.itemType = itemType;
//            this.itemStatus = itemStatus;
//            this.authors = authors;
//        }
//
//        // Has some 'setter-like' methods, one per each OPTIONAL field:
//        //   - each sets the field's value (like a regular setter)
//        //   - but then returns 'this', instead of void (for easier chaining of multiple calls later)
//
//
//        public Builder setRank(String rank) {
//            this.rank = rank;
//            return this;
//        }
//
//        public Builder setPublishingHouse(String publishingHouse) {
//            this.publishingHouse = publishingHouse;
//            return this;
//        }
//
//        public Builder setRecordStore(String recordStore) {
//            this.recordStore = recordStore;
//            return this;
//        }
//
//        public Builder setYear(Date publishedYear) {
//            this.publishedYear = publishedYear;
//            return this;
//        }
//
//        // Has a build() method, which is called at the end of process to actually build
//        //   and return an instance of target class, based on current builder settings,
//        //   possibly also running now some final validations
//        public Item build(){
//            return new Item(id, title, itemType, itemStatus, authors, rank, publishingHouse, recordStore, publishedYear);
//        }
//
//        @Override
//        public boolean equals(Object o) {
//            if (this == o) return true;
//            if (o == null || getClass() != o.getClass()) return false;
//            Builder builder = (Builder) o;
//            return Objects.equals(id, builder.id) &&
//                    Objects.equals(title, builder.title) &&
//                    itemType == builder.itemType &&
//                    itemStatus == builder.itemStatus &&
//                    Objects.equals(authors, builder.authors) &&
//                    Objects.equals(rank, builder.rank) &&
//                    Objects.equals(publishingHouse, builder.publishingHouse) &&
//                    Objects.equals(recordStore, builder.recordStore) &&
//                    Objects.equals(publishedYear, builder.publishedYear);
//        }
//
//        @Override
//        public int hashCode() {
//            return Objects.hash(id, title, itemType, itemStatus, authors, rank, publishingHouse, recordStore, publishedYear);
//        }
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Item item = (Item) o;
//        return Objects.equals(id, item.id) &&
//                Objects.equals(title, item.title) &&
//                Objects.equals(authors, item.authors) &&
//                itemType == item.itemType &&
//                itemStatus == item.itemStatus &&
//                Objects.equals(rank, item.rank) &&
//                Objects.equals(publishingHouse, item.publishingHouse) &&
//                Objects.equals(recordStore, item.recordStore) &&
//                Objects.equals(publishedYear, item.publishedYear);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(id, title, authors, itemType, itemStatus, rank, publishingHouse, recordStore, publishedYear);
//    }
//}
