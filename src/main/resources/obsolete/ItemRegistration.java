//package obsolete;
//
//
//import org.springframework.format.annotation.DateTimeFormat;
//
//import javax.persistence.*;
//import java.util.Date;
//import java.util.Objects;
//
//@Entity
//@Table(name = "item_registration", schema = "wantsome")
//public class ItemRegistration {
//
//    @Id
//    Long id;
//
//    @ManyToOne
//    @JoinColumn(name = "item_id")
//    Item item;
//
//    @ManyToOne
//    @JoinColumn(name = "author_id")
//    Author author;
//
//    @Column(name = "registered_at")
//    @Temporal(TemporalType.DATE)
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    private Date registeredAt;
//
//    /* each book has a bar code, to eliminate the case when we have the same book multiple times
//    when a book is added, a barcode is automatically generated
//    */
//    private Long barcode;
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        ItemRegistration that = (ItemRegistration) o;
//        return Objects.equals(id, that.id) &&
//                Objects.equals(item, that.item) &&
//                Objects.equals(author, that.author) &&
//                Objects.equals(registeredAt, that.registeredAt) &&
//                Objects.equals(barcode, that.barcode);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(id, item, author, registeredAt, barcode);
//    }
//}
