//package obsolete;
//
//import com.spring.boot.OnlineLibrarySpringBoot.model.ItemRegistration;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.Date;
//import java.util.List;
//
//@Repository
//public interface ItemRegistrationRepository extends JpaRepository<ItemRegistration, Long> {
//
//    ItemRegistration findByBarcode(Long barcode);
//
//    List<ItemRegistration> findByRegisteredAt(Date registeredAt);
//}
