//package obsolete;
//
//import com.spring.boot.OnlineLibrarySpringBoot.model.ItemRegistration;
//import org.springframework.stereotype.Service;
//
//import java.util.Date;
//import java.util.List;
//
//@Service
//public interface ItemRegistrationService {
//
//    ItemRegistration findByBarcode(Long barcode);
//
//    List<ItemRegistration> findByRegisteredAt(Date registeredAt);
//
//    ItemRegistration getItemRegistrationById(Long id);
//}
