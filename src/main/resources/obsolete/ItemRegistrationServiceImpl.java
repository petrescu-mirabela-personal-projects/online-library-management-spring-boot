//package obsolete;
//
//import com.spring.boot.OnlineLibrarySpringBoot.model.ItemRegistration;
//import com.spring.boot.OnlineLibrarySpringBoot.repository.ItemRegistrationRepository;
//import com.spring.boot.OnlineLibrarySpringBoot.services.ItemRegistrationService;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.Date;
//import java.util.List;
//
//public class ItemRegistrationServiceImpl implements ItemRegistrationService {
//
//    @Autowired
//    ItemRegistrationRepository itemRegistrationRepository;
//
//    @Override
//    public ItemRegistration findByBarcode(Long barcode) {
//        return itemRegistrationRepository.findByBarcode(barcode);
//    }
//
//    @Override
//    public List<ItemRegistration> findByRegisteredAt(Date registeredAt) {
//        return itemRegistrationRepository.findByRegisteredAt(registeredAt);
//    }
//
//    @Override
//    public ItemRegistration getItemRegistrationById(Long id) {
//        return itemRegistrationRepository.findById(id).orElseThrow(IllegalArgumentException::new);
//    }
//}
